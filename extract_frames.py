#!/usr/bin/env python
"""Extract images from a rosbag."""

import os
import argparse

import cv2

import rosbag
from sensor_msgs.msg import Image
from cv_bridge import CvBridge


def read_caminfo(bag_file: str, img_caminfo_topic:str, depth_caminfo_topic: str, depth_shift=1000.0):
    bag = rosbag.Bag(bag_file, "r")
    gen_img = bag.read_messages(topics={img_caminfo_topic})
    gen_depth = bag.read_messages(topics={depth_caminfo_topic})

    _, img_msg, _ = next(gen_img)
    _, depth_msg, _ = next(gen_depth)
    # caminfo consists of:
    # width height      <-- for image
    # fx fy
    # cx cy
    # width height      <-- for depth
    # fx fy
    # cx cy
    # depth_shift
    return f"""{img_msg.width} {img_msg.height}
{img_msg.K[0]} {img_msg.K[4]}
{img_msg.K[2]} {img_msg.K[5]}
{depth_msg.width} {depth_msg.height}
{depth_msg.K[0]} {depth_msg.K[4]}
{depth_msg.K[2]} {depth_msg.K[5]}
{depth_shift}"""


def main():
    """Extract a folder of images from a rosbag.  """
    parser = argparse.ArgumentParser(
        description="Extract images from a ROS bag.")
    parser.add_argument("bag_file", help="Input ROS bag.")
    parser.add_argument("output_dir", help="Output directory.")
    parser.add_argument("image_topic", help="Image topic.")
    parser.add_argument("image_caminfo_topic", help="Image camera info topic.")
    parser.add_argument("depth_topic", help="Depth Image topic.")
    parser.add_argument("depth_caminfo_topic", help="Depth camera info topic.")

    args = parser.parse_args()

    # make paths
    VGA = "vga"
    DEPTH = "depth"
    VGA_PATH = os.path.join(args.output_dir, VGA)
    DEPTH_PATH = os.path.join(args.output_dir, DEPTH)
    if not os.path.exists(VGA_PATH):
        os.mkdir(VGA_PATH)
    if not os.path.exists(DEPTH_PATH):
        os.mkdir(DEPTH_PATH)

    print("Extract images from %s on topic %s into %s" % (args.bag_file,
                                                          args.image_topic, args.output_dir))

    caminfo = read_caminfo(args.bag_file, args.image_caminfo_topic, args.depth_caminfo_topic)
    CAMINFO_PATH = os.path.join(args.output_dir, "camerainfo.txt")
    with open(CAMINFO_PATH, "w") as f:
        f.write(caminfo)
    print(f"Camera Info: {caminfo}")
    print(f"Wrote caminfo to: {CAMINFO_PATH}")    


    bag = rosbag.Bag(args.bag_file, "r")
    bridge = CvBridge()

    gen_color = bag.read_messages(topics={args.image_topic})
    gen_depth = bag.read_messages(topics={args.depth_topic})

    depth_queue = []
    matches = []
    frame_idx = 0
    while True:
        try:
            _, img, t_img = next(gen_color)
            t_img = t_img.to_sec()
            t_depth = float("inf")
            min_time = float("inf")
            queue_idx = 0
            # get the closest nearset depth message to the current img (in time)
            while True:
                # get a new element. Ether from the queue or the generator
                if queue_idx < len(depth_queue):
                    depth_cand, t_depth_cand = depth_queue[queue_idx]
                else:
                    _, depth_cand, t_depth_cand = next(gen_depth)
                    t_depth_cand = t_depth_cand.to_sec()
                    depth_queue.append((depth_cand, t_depth_cand))

                # if we get further away form the sample: break
                dt = abs(t_depth_cand - t_img)
                if dt < min_time:
                    min_time = dt
                    t_depth = t_depth_cand
                    depth = depth_cand
                    queue_idx += 1
                else:
                    break

            # prune depth queue of all entries earlier than the selected t_depth
            while True:
                _, t_tmp = depth_queue[0]
                # 1 sec approximates the time difference between frames here.
                if t_tmp < min(t_depth, t_img) - 1:
                    depth_queue.pop(0)
                else:
                    break

            img_path = os.path.join(VGA, f"frame{frame_idx:06d}.jpg")
            depth_path = os.path.join(DEPTH, f"frame{frame_idx:06d}.png")

            cv_img = bridge.imgmsg_to_cv2(img, desired_encoding="passthrough")
            cv2.imwrite(os.path.join(args.output_dir, img_path), cv2.cvtColor(
                cv_img, cv2.COLOR_RGB2BGR), [cv2.IMWRITE_JPEG_QUALITY, 90])
            cv_depth = bridge.imgmsg_to_cv2(
                depth, desired_encoding="passthrough")
            cv2.imwrite(os.path.join(args.output_dir, depth_path), cv_depth)

            matches.append((depth_path, img_path))
            print(
                f"Wrote {depth_path}, {img_path}, delta_t: {abs(t_img - t_depth)}s")

            frame_idx += 1
        except StopIteration:
            print(f"Done, wrote {frame_idx + 1} frames")
            break

    # write machings of depth and vga
    MATCH_PATH = os.path.join(args.output_dir, "depth_vga.match")
    with open(MATCH_PATH, "w") as f:
        f.write("\n".join([" ".join(match) for match in matches]))
    print(f"Wrote matching file: {MATCH_PATH}")

    bag.close()
    return


if __name__ == '__main__':
    main()
