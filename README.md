# Extract Rosbag

## Record Data
Start realsense ros:
```bash
roslaunch realsense2_camera rs_aligned_depth.launch align_depth:=true
```

Record the relevant topics in a rosbag:
```bash
rosbag record /camera/color/image_raw /camera/color/camera_info /camera/aligned_depth_to_color/camera_info /camera/aligned_depth_to_color/image_raw -O room.bag
```


## Sync Depth and Camera

Extracts a rosbag with realsense camera data and matches depth to RGB images.
Output is in TextureFusion's data format.

Sync the bag file and extract it with:
```bash
make <bag file>.bag # you can also pass multiple bag files here
```

Re-extract all datasets:
```bash
make -j
```

Extract a specific dataset name
```bash
make <BAG_FILE>.bag
```