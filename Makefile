%.bag: FORCE
	mkdir "extracted" ; \
	cd "extracted" && \
	rm -rf "$*" && \
	mkdir "$*" && \
	cd "$*" && \
	python3 "../../extract_frames.py" \
		"../../$@" \
		"." \
		"/camera/color/image_raw" \
		"/camera/color/camera_info" \
		"/camera/aligned_depth_to_color/image_raw" \
		"/camera/aligned_depth_to_color/camera_info"

all: fast_motion.bag further_away.bag further_away2.bag last_run_shake.bag last_run.bag slow.bag

FORCE: ;